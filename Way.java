import java.util.ArrayList;

public class Way {

	private ArrayList< Commands > cmds;
	private int dir;
	
	public Way() {
		
		dir = 1;
						
		cmds = new ArrayList< Commands >(5);
		cmds.add(null);
		BuilderKeys cmd;
		
		cmd = new BuilderKeys();
		cmd.buildRight();
		cmds.add( cmd.getCommand() );
		
		cmd = new BuilderKeys();
		cmd.buildLeft();
		cmds.add( cmd.getCommand() );		

		cmd = new BuilderKeys();
		cmd.buildUp();
		cmds.add( cmd.getCommand() );

		cmd = new BuilderKeys();
		cmd.buildDown();
		cmds.add( cmd.getCommand() );
		
	}
	
	public int key( int d ){
		return dir = cmds.get(dir).direction(d);
	}
	
	public int getDir() {
		return dir;
	}
	
}
