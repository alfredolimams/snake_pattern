
public class Down implements Order {
	@Override
	public Tuple execute(Tuple head) {
		return head.down(head);
	}
}
