import java.util.ArrayList;

//Controls all the game logic .. most important class in this project.
public class ThreadsController extends Thread {
	ArrayList<ArrayList<DataOfSquare>> Squares = new ArrayList<ArrayList<DataOfSquare>>();
	long speed = 50;
	public static Way way;
	Snake snake;
	Tuple foodPosition;

	// Constructor of ControlleurThread
	ThreadsController() {
		Squares = Window.Grid;
		snake = new Snake();
		way = new Way();
		foodPosition = new Tuple(Window.height - 1, Window.width - 1);
		draw(foodPosition, 1);
	}

	// Important part :
	public void run() {

		while (true) {

			snake.move(way.getDir());
			draw(snake.headSnake(), 0);
			draw(snake.deleteTailSnake(), 2);
			draw(snake.tailSnake(), 0);

			if (snake.eat(foodPosition) && snake.size() != 400) {
				foodPosition = snake.getValAleaNotInSnake();
				draw(foodPosition, 1);
			}

			if (snake.checkCollision() || snake.size() == 400) {
				stopTheGame();
			}

			pauser();
		}
	}

	// delay between each move of the snake
	private void pauser() {
		try {
			sleep(speed);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// Stops The Game
	private void stopTheGame() {
		System.out.println("COLISION! \n");
		while (true) {
			pauser();
		}
	}

	private void draw(Tuple t, int c) {
		Squares.get(t.getY()).get(t.getX()).lightMeUp(c);
	}
}

