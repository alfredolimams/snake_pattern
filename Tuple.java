public class Tuple {
	private int x;
	private int y;

	public Tuple() {
		this.x = (int) (Math.random() * 19);
		this.y = (int) (Math.random() * 19);
	}

	public Tuple(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void ChangeData(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public boolean equals(Tuple other) {
		return this.x == other.getX() && this.y == other.getY();
	}

	public Tuple left(Tuple headSnake) {
		return (new Tuple((headSnake.getX() + 19) % 20, headSnake.getY()));
	}

	public Tuple right(Tuple headSnake) {
		return (new Tuple((headSnake.getX() + 1) % 20, headSnake.getY()));
	}

	public Tuple up(Tuple headSnake) {
		return (new Tuple(headSnake.getX(), (headSnake.getY() + 1) % 20));
	}

	public Tuple down(Tuple headSnake) {
		return (new Tuple(headSnake.getX(), (headSnake.getY() + 19) % 20));
	}
}