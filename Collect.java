import java.util.LinkedList;

public class Collect implements Iterator{
	
	LinkedList<Object> objects;
	int position;
	public Collect() {
		objects = new LinkedList<Object>();
		position = 0;
	}
	public Boolean hasNext() {
		if( position == objects.size() ){
			return false;
		}
		return true;
	}
	@Override
	public Object next() {
		return objects.get(position++);
	} 
}
